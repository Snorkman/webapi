﻿using System.Configuration;

namespace InitPoint.Common.Helpers
{
    /// <summary>
    /// Interface. Describes configuration helper for the application
    /// </summary>
    public class ConfigHelper: IConfigHelper
    {
        public virtual string JobStorageKey
        {
            get
            {
                return GetStringAppSetting("JobStorageKey", "Job");
            }
        }

        protected string GetStringAppSetting(string key, string defaultValue = "")
        {
            var value = ConfigurationManager.AppSettings[key];
            if (string.IsNullOrEmpty(value))
            {
                value = defaultValue;
            }
            return value;
        }
    }
}
