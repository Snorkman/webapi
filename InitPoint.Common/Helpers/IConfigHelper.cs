﻿namespace InitPoint.Common.Helpers
{
    public interface IConfigHelper
    {
        string JobStorageKey { get; }
    }
}
