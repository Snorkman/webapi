﻿using System;
using System.IO;
using System.Text;

namespace InitPoint.Common
{

    /// <summary>
    /// Basic logger to log errors
    /// </summary>
    public class Logger
    {
        /// <summary>
        /// Constructor. Initalizes logger.
        /// </summary>
        /// <param name="directoryPath"></param>
        public Logger(string directoryPath)
        {
            _directoryPath = directoryPath;
            if (!Directory.Exists(_directoryPath))
            {
                Directory.CreateDirectory(_directoryPath);
            }
        }

        #region Public Properties

        /// <summary>
        /// Log file path
        /// </summary>
        public string ErrorLogFile
        {
            get
            {
                var fileName = string.Format("Error_{0}.log", DateTime.UtcNow.ToString("dd_MM_yyyy"));

                return Path.Combine(_directoryPath, fileName);
            }
        }

        /// <summary>
        /// Log file path
        /// </summary>
        public string DebugLogFile
        {
            get
            {
                var fileName = string.Format("Debug_{0}.log", DateTime.UtcNow.ToString("dd_MM_yyyy"));

                return Path.Combine(_directoryPath, fileName);
            }
        }

        #endregion Public Properties

        #region Public Methods

        /// <summary>
        /// Logs the message
        /// </summary>
        /// <param name="message"></param>
        public void LogDebug(string message)
        {
            LogToFile(DebugLogFile, message);
        }

        /// <summary>
        /// Logs an exception
        /// </summary>
        /// <param name="exception"></param>
        public void LogException(string info, Exception exception)
        {
            var errorMessage = GetExceptionMessage(exception);
            var message = string.Format("Info {0}. Error: {1}", info, errorMessage);
            LogError(message);
        }

        #endregion Public Methods


        #region Private Methods

        /// <summary>
        /// Gets exception message
        /// </summary>
        /// <param name="exception"></param>
        /// <param name="tab"></param>
        /// <returns></returns>
        private string GetExceptionMessage(Exception exception, string tab = "")
        {
            var exceptionBuilder = new StringBuilder();
            if (exception != null)
            {
                exceptionBuilder.AppendFormat("{0}Unhandled Exception: {1} \r\n", tab, exception.Message);
                exceptionBuilder.AppendFormat("{0}Stack Trace: {1} \r\n", tab, exception.StackTrace);

                if (exception.InnerException != null)
                {
                    var newTab = string.Format("\t{0}", tab);
                    exceptionBuilder.AppendFormat("{0}Inner Exception information\r\n", tab);
                    exceptionBuilder.AppendFormat("{0} {1}", tab, GetExceptionMessage(exception.InnerException, newTab));
                }
            }
            else
            {
                exceptionBuilder.AppendLine("Exception is null");
            }


            return exceptionBuilder.ToString();
        }

        /// <summary>
        /// Writes the message to file, appends the file.
        /// </summary>
        /// <param name="fileName"></param>
        /// <param name="message"></param>
        private void LogToFile(string fileName, string message)
        {
            var logBuilder = new StringBuilder();

            logBuilder.AppendFormat("[{0} UTC (local)]: {1} \r\n", DateTime.UtcNow, message);

            lock (_lock)
            {
                File.AppendAllText(fileName, logBuilder.ToString());
            }
        }

        /// <summary>
        /// Logs the message
        /// </summary>
        /// <param name="message"></param>
        private void LogError(string message)
        {
            LogToFile(ErrorLogFile, message);
        }

        #endregion Private Methods

        /// <summary>
        /// Directory path where all logs are stored
        /// </summary>
        private readonly string _directoryPath;

        /// <summary>
        /// File writer lock
        /// </summary>
        private readonly object _lock = new object();
    }
}
