﻿using System.Collections.Generic;

namespace DoNotModify.Persistance
{
    public static class Storage
    {
        // Don't spend any time on this.  This is just a plan bit of static object storage should you need it.

        private static readonly Dictionary<string, Dictionary<string, object>> _stores = new Dictionary<string, Dictionary<string, object>>();

        public static Dictionary<string, object> GetStorage(string instanceName)
        {
            if (_stores.ContainsKey(instanceName))
                return _stores[instanceName];

            _stores.Add(instanceName, new Dictionary<string, object>());

            return _stores[instanceName];
        }
    }
}
