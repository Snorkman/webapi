﻿using InitPoint.Service.Models;
using System.Collections.Generic;

namespace InitPoint.Service.Interfaces
{
    public interface IJobService
    {
        List<Job> GetAllJobs();

        Job GetJob(string Id);

        Job CreateJob(Job newJob);

        Job UpdateJob(Job job);

        bool DeleteJob(string id);
    }
}
