﻿using InitPoint.Common.Helpers;
using InitPoint.Domain.DataProviders;
using InitPoint.Domain.Interfaces;
using Ninject;
using Ninject.Modules;

namespace InitPoint.Service.Configuration
{

    /// <summary>
    /// NInject module for services and data providers
    /// </summary>
    public class CoreNinjectModule : NinjectModule
    {
        public override void Load()
        {
            Bind<IJobDataProvider>().ToMethod(ctx =>
            {
                var config = ctx.Kernel.Get<IConfigHelper>();
                var provider = new JobDataProvider(config.JobStorageKey);
                return provider;
            }).InTransientScope();
        }
    }
}
