﻿using AutoMapper;
using InitPoint.Service.Models;

namespace InitPoint.Service.Configuration
{

    /// <summary>
    /// Automapper for fast maps from service model to domain model
    /// </summary>
    public class AutoMapperConfig
    {
        public AutoMapperConfig()
        {
            Config = new MapperConfiguration(cfg =>
            {

                cfg.CreateMap<Job, Domain.Models.Job>();
                cfg.CreateMap<Domain.Models.Job, Job>();


                ApplyAdditionalBindings(cfg);
            });
        }

        protected virtual void ApplyAdditionalBindings(IMapperConfigurationExpression cfg)
        {

        }

        public MapperConfiguration Config { get; set; }
    }
}
