﻿using InitPoint.Domain.Interfaces;
using InitPoint.Service.Helpers;
using InitPoint.Service.Interfaces;
using InitPoint.Service.Models;
using System;
using System.Collections.Generic;

namespace InitPoint.Service.Services
{
    public class JobService : IJobService
    {
        /// <summary>
        /// Basic job service to service job only
        /// </summary>
        /// <param name="initPointDataProvider"></param>
        /// <param name="mapperHepler"></param>
        public JobService(IJobDataProvider initPointDataProvider, IMapperHelper mapperHepler)
        {
            _initPointDataProvider = initPointDataProvider;
            _mapperHepler = mapperHepler;
        }

        public List<Job> GetAllJobs()
        {
            var jobs = _initPointDataProvider.GetAllJobs();
            var mappedJobs = _mapperHepler.MapListModel<Job, Domain.Models.Job>(jobs);
            return mappedJobs;
        }

        public Job GetJob(string Id)
        {
            var domainJob = _initPointDataProvider.GetJob(Id);
            var job = _mapperHepler.MapToModel<Job, Domain.Models.Job>(domainJob);
            return job;
        }

        public Job CreateJob(Job newJob)
        {
            newJob.Id = Guid.NewGuid().ToString();
            var model = _mapperHepler.MapToModel<Domain.Models.Job, Job>(newJob);
            var job = _initPointDataProvider.AddJob(model);
            var mappedJob = _mapperHepler.MapToModel<Job, Domain.Models.Job>(job);
            return mappedJob;
        }

        public Job UpdateJob(Job job)
        {
            var model = _mapperHepler.MapToModel<Domain.Models.Job, Job>(job);

            var getedJob = _initPointDataProvider.GetJob(job.Id);
            getedJob.IsInProgress = !getedJob.IsInProgress;
            getedJob.UpdatesCount++;

            var updatedJob = _initPointDataProvider.UpdateJob(getedJob);
            var mappedJob = _mapperHepler.MapToModel<Job, Domain.Models.Job>(updatedJob);
            return mappedJob;
        }

        public bool DeleteJob(string id)
        {
            var isDeleted = _initPointDataProvider.RemoveJob(id);
            return isDeleted;
        }

        private readonly IJobDataProvider _initPointDataProvider;
        private readonly IMapperHelper _mapperHepler;
    }
}
