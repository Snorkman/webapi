﻿using System.Collections.Generic;

namespace InitPoint.Service.Helpers
{
    public interface IMapperHelper
    {
        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T1">Mapped to</typeparam>
        /// <typeparam name="T2">Mapped from</typeparam>
        /// <param name="job"></param>
        /// <returns>Mapped to</returns>
        T1 MapToModel<T1, T2>(T2 job) where T1 : class where T2 : class;

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T1">Mapped to</typeparam>
        /// <typeparam name="T2">Mapped from</typeparam>
        /// <param name="job"></param>
        /// <returns>Mapped to</returns>
        List<T1> MapListModel<T1, T2>(List<T2> job) where T1 : class where T2 : class;
    }
}
