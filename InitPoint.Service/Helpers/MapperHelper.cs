﻿using AutoMapper;
using System.Collections.Generic;

namespace InitPoint.Service.Helpers
{
    /// <summary>
    /// Mapper helper
    /// </summary>
    public class MapperHelper: IMapperHelper
    {

        public MapperHelper(IMapper mapper)
        {
            _mapper = mapper;
        }
        /// <summary>
        /// Map from one model to another
        /// </summary>
        /// <typeparam name="T1">Map to model</typeparam>
        /// <typeparam name="T2">Map from model</typeparam>
        /// <param name="job"></param>
        /// <returns>T1 model</returns>
        public T1 MapToModel<T1, T2>(T2 job) where T1: class where T2: class
        {
            var model = _mapper.Map<T1>(job);
            return model;
        }

        /// <summary>
        /// Map from one list of models to another
        /// </summary>
        /// <typeparam name="T1">List of models map to</typeparam>
        /// <typeparam name="T2">List of models map from</typeparam>
        /// <param name="job"></param>
        /// <returns>List T1</returns>
        public List<T1> MapListModel<T1, T2>(List<T2> job) where T1 : class where T2 : class
        {
            var model = _mapper.Map<List<T1>>(job);
            return model;
        }

        private readonly IMapper _mapper;
    }
}
