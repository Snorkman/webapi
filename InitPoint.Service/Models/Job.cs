﻿namespace InitPoint.Service.Models
{
    /// <summary>
    /// Service job model
    /// </summary>
    public class Job
    {
        public string Id { get; set; }

        public bool IsInProgress { get; set; }

        public int UpdatesCount { get; set; }
    }
}
