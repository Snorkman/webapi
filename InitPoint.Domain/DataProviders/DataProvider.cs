﻿using DoNotModify.Persistance;
using InitPoint.Domain.Interfaces;
using System.Collections.Generic;

namespace InitPoint.Domain.DataProviders
{

    /// <summary>
    /// Basic data provider with GET, ADD, UPDATE and DELETE methods.
    /// </summary>
    public class DataProvider : IDataProvider
    {
        /// <summary>
        /// For every storage we will have special storage to avoid store different things in one storage. Jobs, pops, ect.
        /// </summary>
        /// <param name="storageKey"></param>
        public DataProvider(string storageKey)
        {
            StorageKey = storageKey;
            _storage = Storage.GetStorage(StorageKey);
        }

        public string StorageKey { get; set; }

        public List<T> GetAllRecords<T>() where T : class
        {
            var records = new List<T>();
            var keys = _storage.Keys;

            foreach(var key in keys)
            {
                object obj;
                _storage.TryGetValue(key, out obj);
                records.Add((T)obj);
            }
            return records;
        }

        public T GetRecord<T>(string key) where T : class
        {
            object result;
            _storage.TryGetValue(key, out result);
            return (T)result;
        }

        public T AddRecord<T>(string key, T obj) where T : class
        {
            _storage.Add(key, obj);
            var newRecord = GetRecord<T>(key);
            return newRecord;
        }

        public bool RemoveRecord(string key)
        {
            var isRemoved =_storage.Remove(key);
            return isRemoved;
        }

        public T UpdateRecord<T>(string key, T obj) where T: class
        {
            var record = GetRecord<T>(key);
            _storage.Remove(key);
            _storage.Add(key, obj);
            var updatedRecord = GetRecord<T>(key);
            return updatedRecord;
        }

        private Dictionary<string, object> _storage;
    }
}
