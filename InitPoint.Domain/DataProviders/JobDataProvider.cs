﻿using DoNotModify.Persistance;
using InitPoint.Domain.Interfaces;
using InitPoint.Domain.Models;
using System.Collections.Generic;

namespace InitPoint.Domain.DataProviders
{

    /// <summary>
    /// Data provider for jobs
    /// </summary>
    public class JobDataProvider: DataProvider, IJobDataProvider
    {

        public JobDataProvider(string storageKey) : base(storageKey)
        { }

        public List<Job> GetAllJobs()
        {
            var jobs = GetAllRecords<Job>();
            return jobs;
        }

        public Job GetJob(string jobId)
        {
            var job = GetRecord<Job>(jobId);
            return job;
        }

        public Job AddJob(Job newJob)
        {
            var addedJob = AddRecord(newJob.Id, newJob);
            return addedJob;
        }

        public Job UpdateJob(Job jobToUpdate)
        {
            var updatedJob = UpdateRecord(jobToUpdate.Id, jobToUpdate);
            return updatedJob;
        }

        public bool RemoveJob(string id)
        {
            var isJobRemoved = RemoveRecord(id);
            return isJobRemoved;
        }
    }
}
