﻿using InitPoint.Domain.Models;
using System.Collections.Generic;

namespace InitPoint.Domain.Interfaces
{
    public interface IJobDataProvider
    {
        List<Job> GetAllJobs();

        Job GetJob(string jobId);

        Job AddJob(Job newJob);

        Job UpdateJob(Job jobToUpdate);

        bool RemoveJob(string id);
    }
}
