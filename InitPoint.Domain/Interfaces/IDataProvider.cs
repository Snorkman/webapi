﻿
namespace InitPoint.Domain.Interfaces
{
    public interface IDataProvider
    {
        string StorageKey { get; set; }
    }
}
