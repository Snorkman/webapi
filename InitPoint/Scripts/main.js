﻿//It were created fast


var updateTable = function (data) {
    $('#table').empty();

    if (data.length > 0) {

        var table = $('<table><tr><th>Id</th><th>In Progress</th><th>Updates Count</th><th>Actions</th></tr></table>').addClass('foo');

        $.each(data, function (index, value) {

            var startText = value.isInProgress ? "Stop Job" : "Start Job";

            var row = $('<tr><td class="jobId"><p>' + value.id + '</p></td><td>' + value.isInProgress + '</td> <td>' + value.updatesCount + '</td><td><button class="jobUpdate">' + startText + '</button><button class="jobDelete">Delete Job</button></td></tr>');
            table.append(row);
        });

        $('#table').append(table);
    }

    var getId = function (e) {
        var $element = $(e.target);
        var id = $element.parents("tr").find(".jobId").find("p").text();
        return id;
    };

    $(".jobDelete").on("click", function (e) {
        var id = getId(e);
        deleteJob(id);
    });


    $(".jobUpdate").on("click", function (e) {
        var id = getId(e);
        var data = {
            id: id
        };

        updateJob(data);
    });
};

var getAllJobs = function () {
    $.get("/api/jobs", function (data, status) {
        updateTable(data);
    });
};

var addJob = function () {

    $.ajax({
        url: "/api/jobs",
        type: 'POST',
        data: { isInProgress: true },
        success: function (result) {
            getAllJobs();
        }
    });

};

var updateJob = function (data) {
    $.ajax({
        url: "/api/jobs",
        type: 'PUT',
        data: data,
        success: function (result) {
            getAllJobs();
        }
    });
};

var deleteJob = function (id) {

    $.ajax({
        url: "/api/jobs/" + id,
        type: 'DELETE',
        success: function (result) {
            if (result) {
                getAllJobs();
            };
        }
    });

};

$(".add-job").on("click", function () {
    addJob();
});

getAllJobs();