﻿using InitPoint.Common;
using Ninject;
using System.Net;
using System.Net.Http;
using System.ServiceModel.Channels;
using System.Web;
using System.Web.Http.Filters;

namespace InitPoint.Framework
{

    /// <summary>
    /// Framework to log all errors
    /// </summary>
    public class ErrorFilterAttribute : ExceptionFilterAttribute
    {


        public const string DefaultMessage = "Error was occured. Please contact with Administrator";

        public string ErrorMessage { get; set; }

        [Inject]
        public Logger Logger { get; set; }

        public override void OnException(HttpActionExecutedContext actionExecutedContext)
        {

            base.OnException(actionExecutedContext);

            var info = string.Format("Request IP Address {0} \r\n", GetClientIp(actionExecutedContext.Request));

            Logger.LogException(info, actionExecutedContext.Exception);

            actionExecutedContext.Response = actionExecutedContext.Request.CreateErrorResponse(HttpStatusCode.InternalServerError,
                string.IsNullOrEmpty(ErrorMessage)
                    ? DefaultMessage
                    : ErrorMessage);
        }

        private string GetClientIp(HttpRequestMessage request)
        {
            if (request.Properties.ContainsKey("MS_HttpContext"))
            {
                return ((HttpContextWrapper)request.Properties["MS_HttpContext"]).Request.UserHostAddress;
            }

            if (request.Properties.ContainsKey(RemoteEndpointMessageProperty.Name))
            {
                RemoteEndpointMessageProperty prop;
                prop = (RemoteEndpointMessageProperty)request.Properties[RemoteEndpointMessageProperty.Name];
                return prop.Address;
            }

            return null;
        }

    }
}