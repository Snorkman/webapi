﻿using InitPoint.Service.Interfaces;
using InitPoint.Service.Models;
using System.Net;
using System.Net.Http;

namespace InitPoint.Controllers.Api
{
    /// <summary>
    /// Jobs controllers
    /// </summary>
    public class JobsController : BaseApiController
    {

        public JobsController(IJobService jobService)
        {
            _jobService = jobService;
        }

        public HttpResponseMessage Get()
        {
            var jobs = _jobService.GetAllJobs();
            return Request.CreateResponse(HttpStatusCode.OK, jobs);
        }

        public HttpResponseMessage Get(string id)
        {
            var job = _jobService.GetJob(id);
            return Request.CreateResponse(HttpStatusCode.OK, job);
        }

        public HttpResponseMessage Post(Job model)
        {
            var newJob = _jobService.CreateJob(model);

            return Request.CreateResponse(HttpStatusCode.OK, newJob);
        }

        public HttpResponseMessage Put(Job model)
        {
            var updatedJob = _jobService.UpdateJob(model);

            return Request.CreateResponse(HttpStatusCode.OK, updatedJob);
        }

        public HttpResponseMessage Delete(string id)
        {
            var isJobDeleted = _jobService.DeleteJob(id);
            return Request.CreateResponse(HttpStatusCode.OK, isJobDeleted);
        }

        private readonly IJobService _jobService;
    }
}
