using InitPoint.Framework;
using System.Web.Http;

namespace InitPoint.Controllers.Api
{
    /// <summary>
    /// Base controller for all APIs controllers
    /// </summary>
    [ErrorFilter]
    public class BaseApiController : ApiController
    {
    }
}
