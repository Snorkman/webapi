﻿using System.Web.Mvc;

namespace InitPoint.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }
    }
}