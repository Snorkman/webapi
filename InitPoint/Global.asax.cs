﻿using InitPoint.App_Start;
using Newtonsoft.Json.Serialization;
using System;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Routing;

namespace InitPoint
{
    public class WebApiApplication : HttpApplication
    {
        private const string ReturnUrlRegexPattern = @"\?ReturnUrl=.*$";

        public WebApiApplication()
        {
            PreSendRequestHeaders += MvcApplicationOnPreSendRequestHeaders;
        }

        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            GlobalConfiguration.Configuration.Formatters.XmlFormatter.SupportedMediaTypes.Clear();

            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            WebApiConfig.Register(GlobalConfiguration.Configuration);
            RouteConfig.RegisterRoutes(RouteTable.Routes);



            GlobalConfiguration.Configuration
                  .Formatters
                  .JsonFormatter
                  .SerializerSettings
                  .ContractResolver = new CamelCasePropertyNamesContractResolver();
            GlobalConfiguration.Configuration.EnsureInitialized();

        }

        private void MvcApplicationOnPreSendRequestHeaders(object sender, EventArgs e)
        {
            string redirectUrl = Response.RedirectLocation;
            if (string.IsNullOrEmpty(redirectUrl) || !Regex.IsMatch(redirectUrl, ReturnUrlRegexPattern))
            {
                return;
            }

            Response.RedirectLocation = Regex.Replace(redirectUrl, ReturnUrlRegexPattern, string.Empty);

        }
    }
}