﻿using AutoMapper;
using InitPoint.Common;
using InitPoint.Common.Helpers;
using InitPoint.Domain.DataProviders;
using InitPoint.Domain.Interfaces;
using InitPoint.Service.Configuration;
using InitPoint.Service.Helpers;
using InitPoint.Service.Interfaces;
using InitPoint.Service.Services;
using Ninject;
using Ninject.Web.Common;
using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Http.Dependencies;

namespace InitPoint.App_Start
{
    public class NinjectDependencyResolver : IDependencyResolver
    {
        private IKernel _kernel;
        public NinjectDependencyResolver(IKernel kernelParam)
        {
            _kernel = kernelParam;
            AddBindings();
        }
        public object GetService(Type serviceType)
        {
            return _kernel.TryGet(serviceType);
        }
        public IEnumerable<object> GetServices(Type serviceType)
        {
            return _kernel.GetAll(serviceType);
        }


        private void AddBindings()
        {

            //Helpers
            _kernel.Bind<IConfigHelper>().To<ConfigHelper>().InRequestScope();
            _kernel.Bind<IMapperHelper>().To<MapperHelper>().InRequestScope();

            //Services
            _kernel.Bind<IJobService>().To<JobService>().InTransientScope();

            //Automapper
            _kernel.Bind<IMapper>().ToMethod(ctx =>
            {
                var configurator = ctx.Kernel.Get<AutoMapperConfig>();
                var mapper = configurator.Config.CreateMapper();
                return mapper;
            }).InSingletonScope();

            //Logger
            _kernel.Bind<Logger>().ToMethod(ctx =>
            {
                return new Logger(HttpContext.Current.Server.MapPath("~/Logs"));
            }).InSingletonScope();
        }

        public void Dispose() { }

        public IDependencyScope BeginScope()
        {
            return this;
        }
    }
}