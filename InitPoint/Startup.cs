﻿using Microsoft.Owin;
using Owin;
using System.Web.Http;
using Newtonsoft.Json;
using Swashbuckle.Application;
using System.Linq;

[assembly: OwinStartup(typeof(InitPoint.Startup))]

namespace InitPoint
{
    public class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            HttpConfiguration config = new HttpConfiguration();
            config.MapHttpAttributeRoutes();
            
            JsonSerializerSettings jsonSetting = new JsonSerializerSettings();
            jsonSetting.Converters.Add(new Newtonsoft.Json.Converters.StringEnumConverter());
            config.Formatters.JsonFormatter.SerializerSettings = jsonSetting;

            config.EnableSwagger(
                c => {
                    c.SingleApiVersion("v1", "InitPoint");
                    c.ResolveConflictingActions(apiDescriptions => apiDescriptions.First());
                    c.DescribeAllEnumsAsStrings();
                })
                .EnableSwaggerUi();

            app.UseWebApi(config);
        }
    }
}
